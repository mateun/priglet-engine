#version 430 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 uvs;
layout (location = 2) in vec3 normals;


layout(location = 3) uniform mat4 mat_model;
layout(location = 4) uniform mat4 mat_view;
layout(location = 5) uniform mat4 mat_proj;

layout(location = 6) uniform mat4 mat_view_light;
layout(location = 7) uniform mat4 mat_proj_light;

out vec2 fs_uvs;
out vec3 fs_normals;
out vec4 fs_frag_pos_light;

void main() {
  gl_Position = mat_proj * mat_view * mat_model  * (vec4(position, 1));
  fs_uvs = uvs;
  fs_normals = normalize(vec3(mat_model * vec4(normals, 0.0)));
  fs_frag_pos_light = mat_proj_light * mat_view_light * mat_model * (vec4(position, 1));

}

