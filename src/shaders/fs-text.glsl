#version 430 core

layout(binding = 0) uniform sampler2D sampler;
layout(binding = 1) uniform sampler2D shadowMapSampler;
uniform bool isLit;
uniform bool isFixColored;
uniform float alpha;
uniform vec3 tint;
uniform vec3 sunDirection;

out vec4 color;
in vec2 fs_uvs;
in vec3 fs_normals;
in vec4 fs_frag_pos_light;



void main() {
	
	color = texture(sampler, fs_uvs);
	color *= vec4(tint, 1);
	color *= alpha;

	


	
}
