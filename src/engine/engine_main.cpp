#include <stdio.h>
#include <window.h>
#include <WinUser.h>
#include <SDL2/SDL_events.h>
#include <vector>
#include <game_lib.h>
#include "lua_integration.h"
#include "game_lib_mangement.h"
#include <assetpipeline.h>
#include <engine.h>
#include <shader.h>
#include <shader_cache.h>
#include <model_import.h>

static const std::string shaderBaseDir = "e:/Projects/c++/piglet_engine/src/shaders/";

void initShaders() {
	GLuint vs = createVertexShader(shaderBaseDir + "vs.glsl");
	GLuint fs = createFragmentShader(shaderBaseDir + "fs.glsl");
	GLuint shaderProg = createShaderProgram(vs, fs);

	GLuint shadowmap_vs = createVertexShader(shaderBaseDir + "shadowmap-vs.glsl");
	GLuint shadowmap_fs = createFragmentShader(shaderBaseDir + "shadowmap-fs.glsl");
	GLuint shadowMapshaderProg = createShaderProgram(shadowmap_vs, shadowmap_fs);

	GLuint fsText = createFragmentShader(shaderBaseDir + "fs-text.glsl");
	GLuint textRenderShaderProg = createShaderProgram(vs, fsText);

	getShaderCache()->put("default", shaderProg);
	getShaderCache()->put("shadowmap", shadowMapshaderProg);
	getShaderCache()->put("text", textRenderShaderProg);
}

int main(int argc, char** args) {
	printf("piglet engine 0.0.1\n");

	importSkeletalToVAO("e:/temp/assets/human_rigged_2.fbx");

	//initLua();
	//std::string update_lua_code = readLuadCode("e:/temp/update.lua");
	//runCode(update_lua_code);
	//runCodeFromFile("e:/temp/update.lua");

	
	createWindow(800, 600, false, true);
	HWND hwnd = getNativeWindow();

	initShaders();


	AssetDatabase assetDB("e:/temp/king_assets");
	assetDB.importNewAssets();
	getEngine()->assetDatabase = &assetDB;

	GameDLL game("E:/Projects/C++/koenig_piglet_game/build/Debug");
	game.init(getEngine());

	
	SDL_Event event;

	std::vector<SDL_Event> frameEvents;

	Timer timer_gross;
	Timer timer_net;

	bool gameRunning = true;
	while (gameRunning) {
		timer_net.start();
		frameEvents.clear();
		SDL_Event event;

		while (SDL_PollEvent(&event) != 0) {
			if (event.type == SDL_QUIT) {
				gameRunning = false;
				break;
			}


			if (event.type == SDL_KEYDOWN) {
				if (event.key.keysym.sym == SDLK_ESCAPE) {
					gameRunning = false;
					break;
				}

				if (event.key.keysym.sym == SDLK_f) {
					// TODO 
				}

				if (event.key.keysym.sym == SDLK_r) {
				
				}
			}

			frameEvents.push_back(event);
		}

		//updateUIState(frameEvents);
		
		// TODO: we change this to let the game return a scene pointer each freame
		// The engine then understands this scene and renders the contents of the scene accordingly.
		// This enables the engine to have statistics, centralize light and shadow management, scene and object management
		// etc. 
		game.update(timer_gross.get_last_measure(microseconds), frameEvents);
		game.render(timer_gross.get_last_measure(microseconds));
		
		timer_net.stop();

		flipBuffer();
		timer_gross.stop();
		timer_gross.start();

	}
	
	return 0;
}