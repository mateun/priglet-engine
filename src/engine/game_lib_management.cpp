#include <Windows.h>
#include "game_lib_mangement.h"



GameDLL::GameDLL(const std::string& dllPath) {
	HMODULE game = LoadLibraryA("E:/Projects/C++/koenig_piglet_game/build/Debug/game.dll");
	if (game) {
		game_update = (GAME_UPDATE) GetProcAddress(game, "update");
		game_render = (GAME_RENDER) GetProcAddress(game, "render");
		game_init =   (GAME_INIT) GetProcAddress(game, "init");
	}

	if (!game_update || !game_render || !game_init) {
		MessageBoxA(0, "Could not find  update/render methods.", "Error!", MB_OK | MB_ICONERROR);
		exit(1);
	}

}

void GameDLL::update(float dt, std::vector<SDL_Event> frameEvents)
{
	(game_update)(dt, frameEvents);
}

void GameDLL::render(float dt)
{
	(game_render)(dt);
}

void GameDLL::init(Engine* engine) {
	(game_init)(engine);
}


