#include "lua_integration.h"
#include <string>
#include <fstream> 
#include <iostream> 
#include <sstream>
#include <SDL2/SDL.h>
#include <game_lib.h>


static lua_State* L;

std::string readLuadCode(const std::string& file) {
    std::ifstream inFile(file, std::ios::in);
    if (!inFile) {
        return 0;
    }

    std::ostringstream code;
    while (inFile.good()) {
        int c = inFile.get();
        if (!inFile.eof()) code << (char)c;
    }
    inFile.close();

    return code.str();
}

void initLua() {
	L = luaL_newstate();
	luaL_openlibs(L);

}

void runCode(const std::string& code) {
    //char* code = "print('Hello, World')";

    // Here we load the string and use lua_pcall for run the code
    Timer t;
    t.start();
    if (luaL_loadstring(L, code.c_str()) == LUA_OK) {
        t.stop();
        SDL_Log("script load: %s", std::to_string(t.get_last_measure(microseconds)).c_str());
        if (lua_pcall(L, 0, 1, 0) == LUA_OK) {
            // If it was executed successfuly we 
            // remove the code from the stack
            lua_pop(L, lua_gettop(L));
        }
    }
}

void runFunc(const std::string& funcName) {
    lua_getglobal(L, funcName.c_str());
    lua_pcall(L, 0, 0, 0);
}

void runCodeFromFile(const std::string& fileName)
{
    Timer t;
    t.start();
    if (luaL_loadfile(L, fileName.c_str()) == LUA_OK) {
        t.stop();
        SDL_Log("script load: %s", std::to_string(t.get_last_measure(microseconds)).c_str());
        if (lua_pcall(L, 0, 1, 0) == LUA_OK) {
            lua_pop(L, lua_gettop(L));
        }
        else {
            if (lua_isstring(L, -1)) {
                const char* message = lua_tostring(L, -1);
                lua_pop(L, 1);
                printf("Message from lua: %s\n", message);
            }
        }
    }
}

void cleanupLua() {
	lua_close(L);
}

