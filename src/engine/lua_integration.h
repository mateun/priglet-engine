#pragma once
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#include <string>


void initLua();
void cleanupLua();

std::string readLuadCode(const std::string& file);
void runFunc(const std::string& funcName);
void runCode(const std::string& code);
void runCodeFromFile(const std::string& fileName);

