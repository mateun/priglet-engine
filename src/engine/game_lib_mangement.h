#pragma once
#include <vector>
#include <SDL2/SDL_events.h>
#include <string>
#include <engine.h>

typedef void(__cdecl* GAME_UPDATE)(float, std::vector<SDL_Event>);
typedef void(__cdecl* GAME_RENDER)(float);
typedef void(__cdecl* GAME_UPDATE)(float, std::vector<SDL_Event>);
typedef void(__cdecl* GAME_INIT)(Engine*);


class GameDLL {
public:
	GameDLL(const std::string& dllPath);
	void update(float dt, std::vector<SDL_Event>);
	void render(float dt);
	void init(Engine*);

private:
	GAME_UPDATE game_update;
	GAME_RENDER game_render;
	GAME_INIT game_init;
	HMODULE game;
};
