#pragma once
#include <glm/glm.hpp>

class Camera;

/*
* Axis aligned bounding box
* which ignores the height dimension.
* Collsision tests can be done in the slice
* of a left and right (positive) x value and
* a top and bottom (positive z direction).
*/
struct AABBNoHeight {
	float left;
	float right;
	float top;
	float bottom;
};

struct Ray {
	glm::vec3 origin;
	glm::vec3 direction;
	float maxLength;
};

Ray createRayFromScreenCoordinates(int sx, int sy, Camera* camera, int ScreenWidth, int ScreenHeight);

bool rayIntersectsPlane(glm::vec3 planeNormal, glm::vec3 planePoint, Ray ray, glm::vec3& intersectionPoint);



