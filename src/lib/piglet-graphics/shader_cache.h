#pragma once
#include <string>
#include <map>
#include <GL/glew.h>
#include "pgraph_api.h"


class PIGLETGRAPHICS_API ShaderCache {

public:

	GLuint get(const std::string& name);
	void put(const std::string& name, GLuint shader);

private:
	std::map<std::string, GLuint>	 _shaders;

};

PIGLETGRAPHICS_API ShaderCache* getShaderCache();
