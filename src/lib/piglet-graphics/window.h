#pragma once
#include <pgraph_api.h>
#include <wtypes.h>
#include <string>

enum Event {
	QUIT, 
	KEYPRESS,
	MOUSEMOVE, 
	NONE
};

PIGLETGRAPHICS_API int getScreenWidth();
PIGLETGRAPHICS_API int getScreenHeight();
PIGLETGRAPHICS_API HWND getNativeWindow();

PIGLETGRAPHICS_API void createWindow(int w, int h, bool fullScreen, bool vSync = true);
PIGLETGRAPHICS_API void createWindow(int w, int h, bool fullScreen, const std::string& title, bool vSync = true);
PIGLETGRAPHICS_API void destroyWindow();
PIGLETGRAPHICS_API Event pollEvent();
PIGLETGRAPHICS_API void flipBuffer();
	
