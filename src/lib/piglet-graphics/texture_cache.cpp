#include "texture_cache.h"
#include <map>


static std::map<std::string, GLuint>* _textureCache;

std::map<std::string, GLuint>* getTextureCache() {
	if (!_textureCache) {
		_textureCache = new std::map<std::string, GLuint>(); 
	}

	return _textureCache;
}
