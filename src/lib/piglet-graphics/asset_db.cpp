#include "assetpipeline.h"
#include <filesystem>
#include <iostream>
#include <model_import.h>
#include <ImageImporter.h>
#include <texture.h>

namespace fs = std::filesystem;

AssetDatabase::AssetDatabase(const std::string& assetPath) : _assetPath(assetPath) {

}

void AssetDatabase::importNewAssets()
{
	for (const auto& entry : fs::directory_iterator(_assetPath)) {
		std::cout << entry.path() << std::endl;
		if (entry.path().extension().string() == ".obj") {
			printf("this is a static mesh!\n");
			// Check if we already have an import for this mesh: 
			if (!_staticMeshes[entry.path().stem().string()]) {
				Mesh* m = importToVAO(entry.path().string());
				StaticMesh* sm = new StaticMesh{ m };
				_staticMeshes[entry.path().stem().string()] = sm;
			}
			
		}
		else if (entry.path().extension().string() == ".png" ) {
			printf("importing .png as texture.\n");

			ImageData* lpPalSurface = new ImageData();
			ImageImporter::importPNGFile(entry.path().string(), lpPalSurface);
			GLuint lpPalTexture = createTextureFromSurface(lpPalSurface, true);
			//(*getTextureCache())["lp-palette"] = lpPalTexture;
			_textures[entry.path().stem().string()] = new PTexture{ lpPalTexture };
			delete(lpPalSurface->pixels);
			delete(lpPalSurface);

		}

	}
}

template<>
PIGLETGRAPHICS_API SkeletalMesh* AssetDatabase::get<SkeletalMesh>(const std::string& name) {
	return _skeletalMeshes[name];
}

template<>
PIGLETGRAPHICS_API StaticMesh* AssetDatabase::get<StaticMesh>(const std::string& name) {
	return _staticMeshes[name];
}

template<>
PIGLETGRAPHICS_API PTexture* AssetDatabase::get<PTexture>(const std::string& name) {
	return _textures[name];
}