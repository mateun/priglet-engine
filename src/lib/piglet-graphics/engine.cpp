#include "engine.h"

static Engine* theEngine;

Engine* getEngine() {
	if (!theEngine) {
		theEngine = new Engine();
	}
	return theEngine;
}