#include "model_import.h"
#include <string>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <GL/glew.h>
#include <vector>
#include "render.h"
#include "mesh.h"

AABBNoHeight calculateAABB(const std::vector<float>positions) {
	// The most left vertex
	float leftX = 0;
	for (int i = 0; i < positions.size(); i+=3) {
		float x = positions[i];
		if (x < leftX) {
			leftX = x;
		}
	}

	// The rightmost vertex x value
	float rightX = 0;
	for (int i = 0; i < positions.size(); i += 3) {
		float x = positions[i];
		if (x > rightX) {
			rightX = x;
		}
	}

	// The topmost Z value
	// is the most negative one.
	float topZ = 0;
	for (int i = 0; i < positions.size(); i += 3) {
		float z = positions[i+2];
		if (z < topZ) {
			topZ = z;
		}
	}

	// The bottom Z is the most positive Z value.
	float bottomZ = 0;
	for (int i = 0; i < positions.size(); i += 3) {
		float z = positions[i + 2];
		if (z > bottomZ) {
			bottomZ = z;
		}
	}


	// todo(gru) implement!!
	AABBNoHeight aabb;
	aabb.left = leftX;
	aabb.right = rightX;
	aabb.top = topZ;
	aabb.bottom = bottomZ;

	return aabb;
}

Mesh* importToVAO(const std::string& modelName) {
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(modelName, 
		aiProcess_Triangulate
	//	| aiProcess_FlipUVs
	);

	if (scene == nullptr) {
		printf("error in import of file %s\n", modelName.c_str());
		exit(1);
	}

	std::vector<float> positions;
	std::vector<float> uvs;
	std::vector<float> normals;
	std::vector<int> indices;

	auto* mesh = scene->mMeshes[0];

	for (int x = 0; x < mesh->mNumVertices;x++)
	{
		aiVector3D pos = mesh->mVertices[x];
		positions.push_back(pos.x);
		positions.push_back(pos.y);
		positions.push_back(pos.z);

		if (mesh->HasNormals())
		{
			aiVector3D normal = mesh->mNormals[x];
			normals.push_back(normal.x);
			normals.push_back(normal.y);
			normals.push_back(normal.z);
		}

		if (mesh->HasTextureCoords(0))
		{
			aiVector3D uv = mesh->mTextureCoords[0][x];
			uvs.push_back(uv.x);
			uvs.push_back(uv.y);
		}
	}

	for (int f = 0; f < mesh->mNumFaces; f++)
	{
		aiFace face = mesh->mFaces[f];
		indices.push_back(face.mIndices[0]);
		indices.push_back(face.mIndices[1]);
		indices.push_back(face.mIndices[2]);
	}
			

	// Material and texture import
	/*aiMaterial* aimat = scene->mMaterials[mesh->mMaterialIndex];
	ggraphics::Material* material = new ggraphics::Material();
	material->name = std::string(aimat->GetName().data);
	aiString texturePath;
	std::string basePath = getModelBasePath(fileName);
	aiReturn ret = aimat->GetTexture(aiTextureType::aiTextureType_DIFFUSE, 0, &texturePath, NULL, NULL, NULL, NULL, NULL);
	material->texture = new ggraphics::Texture(std::string(basePath + "/" + texturePath.C_Str()), vec3(0));
	myMesh.material = material;

	model.meshes.push_back(myMesh);*/

	Mesh* rmesh = new Mesh();
	rmesh->numberOfVertices = mesh->mNumVertices;
	rmesh->vao = createVAO(positions, uvs, normals, indices).vao;
	rmesh->aabb = calculateAABB(positions);
	return rmesh;


}

static int numTabs = 0;


void PrintTabs() {
	for (int i = 0; i < numTabs; i++)
		printf("\t");
}

/**
   * Return a string-based representation based on the attribute type.
   */
FbxString GetAttributeTypeName(FbxNodeAttribute::EType type) {
	switch (type) {
	case FbxNodeAttribute::eUnknown: return "unidentified";
	case FbxNodeAttribute::eNull: return "null";
	case FbxNodeAttribute::eMarker: return "marker";
	case FbxNodeAttribute::eSkeleton: return "skeleton";
	case FbxNodeAttribute::eMesh: return "mesh";
	case FbxNodeAttribute::eNurbs: return "nurbs";
	case FbxNodeAttribute::ePatch: return "patch";
	case FbxNodeAttribute::eCamera: return "camera";
	case FbxNodeAttribute::eCameraStereo: return "stereo";
	case FbxNodeAttribute::eCameraSwitcher: return "camera switcher";
	case FbxNodeAttribute::eLight: return "light";
	case FbxNodeAttribute::eOpticalReference: return "optical reference";
	case FbxNodeAttribute::eOpticalMarker: return "marker";
	case FbxNodeAttribute::eNurbsCurve: return "nurbs curve";
	case FbxNodeAttribute::eTrimNurbsSurface: return "trim nurbs surface";
	case FbxNodeAttribute::eBoundary: return "boundary";
	case FbxNodeAttribute::eNurbsSurface: return "nurbs surface";
	case FbxNodeAttribute::eShape: return "shape";
	case FbxNodeAttribute::eLODGroup: return "lodgroup";
	case FbxNodeAttribute::eSubDiv: return "subdiv";
	default: return "unknown";
	}
}

/**
 * Print an attribute.
 */
void PrintAttribute(FbxNodeAttribute* pAttribute) {
	if (!pAttribute) return;

	FbxString typeName = GetAttributeTypeName(pAttribute->GetAttributeType());
	FbxString attrName = pAttribute->GetName();
	PrintTabs();
	// Note: to retrieve the character array of a FbxString, use its Buffer() method.
	printf("<attribute type='%s' name='%s'/>\n", typeName.Buffer(), attrName.Buffer());
}

/**
 * Print a node, its attributes, and all its children recursively.
 */
void PrintNode(FbxNode* pNode) {
	PrintTabs();
	const char* nodeName = pNode->GetName();
	FbxDouble3 translation = pNode->LclTranslation.Get();
	FbxDouble3 rotation = pNode->LclRotation.Get();
	FbxDouble3 scaling = pNode->LclScaling.Get();

	// Print the contents of the node.
	printf("<node name='%s' translation='(%f, %f, %f)' rotation='(%f, %f, %f)' scaling='(%f, %f, %f)'>\n",
		nodeName,
		translation[0], translation[1], translation[2],
		rotation[0], rotation[1], rotation[2],
		scaling[0], scaling[1], scaling[2]
	);
	numTabs++;

	// Print the node's attributes.
	for (int i = 0; i < pNode->GetNodeAttributeCount(); i++)
		PrintAttribute(pNode->GetNodeAttributeByIndex(i));

	// Recursively print the children.
	for (int j = 0; j < pNode->GetChildCount(); j++)
		PrintNode(pNode->GetChild(j));

	numTabs--;
	PrintTabs();
	printf("</node>\n");
}


Mesh* importSkeletalToVAO(const std::string& fileName)
{
	FbxManager* fbxManager = FbxManager::Create();

	// Create the IO settings object.
	FbxIOSettings* ios = FbxIOSettings::Create(fbxManager, IOSROOT);
	fbxManager->SetIOSettings(ios);

	// Create an importer using the SDK manager.
	FbxImporter* lImporter = FbxImporter::Create(fbxManager, "");

	// Use the first argument as the filename for the importer.
	if (!lImporter->Initialize(fileName.c_str(), -1, fbxManager->GetIOSettings())) {
		printf("Call to FbxImporter::Initialize() failed.\n");
		printf("Error returned: %s\n\n", lImporter->GetStatus().GetErrorString());
		exit(-1);
	}

	FbxScene* lScene = FbxScene::Create(fbxManager, "myScene");

	// Import the contents of the file into the scene.
	lImporter->Import(lScene);

	// Print the nodes of the scene and their attributes recursively.
	// Note that we are not printing the root node because it should
	// not contain any attributes.
	FbxNode* lRootNode = lScene->GetRootNode();
	if (lRootNode) {
		for (int i = 0; i < lRootNode->GetChildCount(); i++)
			PrintNode(lRootNode->GetChild(i));
	}

	// Extract animations
	FbxAnimStack* animStack = lScene->GetCurrentAnimationStack();
	int animLayers = animStack->GetMemberCount(FBX_TYPE(FbxAnimLayer));
	

	// The file is imported, so get rid of the importer.
		lImporter->Destroy();


	return nullptr;
}


