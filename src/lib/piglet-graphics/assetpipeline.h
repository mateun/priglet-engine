#pragma once
#include <pgraph_api.h>
#include <string>
#include <map>
#include <mesh.h>
#include <texture.h>


 struct PIGLETGRAPHICS_API SkeletalMesh {

};

 struct PIGLETGRAPHICS_API StaticMesh {
	 Mesh* mesh;
};

struct PIGLETGRAPHICS_API PTexture {
	GLuint texture;
};

class PIGLETGRAPHICS_API AssetDatabase {
public:
	AssetDatabase(const std::string& assetPath);
	void importNewAssets();

	template<typename T> 
	T* get(const std::string& assetName);

private:
	std::map<std::string, SkeletalMesh*> _skeletalMeshes;
	std::map<std::string, StaticMesh*> _staticMeshes;
	std::map<std::string, PTexture*> _textures;
	std::string _assetPath;
};
