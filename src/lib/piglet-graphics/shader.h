#pragma once
#include <GL/glew.h>
#include <string>
#include "pgraph_api.h"


PIGLETGRAPHICS_API GLuint createVertexShader(const std::string& vsFileName);
PIGLETGRAPHICS_API GLuint createFragmentShader(const std::string& psFileName);
PIGLETGRAPHICS_API GLuint createShaderProgram(GLuint vertexShader, GLuint fragmentShader);

PIGLETGRAPHICS_API GLuint createVertexShaderFromCode(const std::string& vsCode);
PIGLETGRAPHICS_API GLuint createFragmentShaderFromCode(const std::string& psCode);

