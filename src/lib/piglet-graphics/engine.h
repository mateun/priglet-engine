#pragma once
#include <assetpipeline.h>
#include "pgraph_api.h"

struct PIGLETGRAPHICS_API Engine {
	AssetDatabase* assetDatabase;
	// Here come other engine singletons
};

PIGLETGRAPHICS_API Engine* getEngine();