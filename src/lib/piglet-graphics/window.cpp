#include "window.h"
#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>
#include <stdio.h>
#include <wtypes.h>


static SDL_Window* window;
static SDL_GLContext glContext;
static int screenWidth;
static int screenHeight;

int getScreenWidth() { return screenWidth; }
int getScreenHeight() { return screenHeight; }

void destroyWindow() {
    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(window);
}

HWND getNativeWindow() {
	SDL_SysWMinfo wmInfo;
	SDL_VERSION(&wmInfo.version);
	SDL_GetWindowWMInfo(window, &wmInfo);
	return wmInfo.info.win.window;
}

void createWindow(int width, int height, bool fullScreen, bool vSync) {
	createWindow(width, height, fullScreen, "Game", vSync);
}

void createWindow(int width, int height, bool fullScreen, const std::string& title, bool vSync) {
	if(SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("error\n"); 
		exit(1);
	}


    window = SDL_CreateWindow(title.c_str(), 100, 100, width, height, SDL_WINDOW_OPENGL);
	if (!window) {
		printf("window error!\n");
		exit(2);
	}
	

	screenWidth = width;
	screenHeight = height;

	printf("window created.\n");

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	glContext = SDL_GL_CreateContext(window);

	if (vSync) {
		SDL_GL_SetSwapInterval(1);
	}
	else {
		SDL_GL_SetSwapInterval(0);
	}
	

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) {
		printf("error initializing glew.%s\n", glewGetErrorString(err));
		exit(3);
	}

	GLint majVer, minVer;
	glGetIntegerv(GL_MAJOR_VERSION, &majVer);
	glGetIntegerv(GL_MINOR_VERSION, &minVer);
	printf("GL Version %d.%d\n", majVer, minVer);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);



}


Event pollEvent() {
	SDL_Event event;
	SDL_PollEvent(&event);
	if (event.type == SDL_QUIT) {
		return QUIT;
	}
	if (event.type == SDL_KEYDOWN) {
	  return KEYPRESS;
	}				    

	return NONE;
}

void flipBuffer()  {
	SDL_GL_SwapWindow(window);

}
