#pragma once
#include <string>
#include <GL/glew.h>
#include "mesh.h"
#include <fbxsdk.h>
#include "pgraph_api.h"

PIGLETGRAPHICS_API Mesh* importToVAO(const std::string& fileName);

PIGLETGRAPHICS_API Mesh* importSkeletalToVAO(const std::string& fileName);
