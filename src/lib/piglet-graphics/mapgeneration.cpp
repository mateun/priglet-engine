#pragma once
#include <SDL2/SDL.h>
#include "mapgeneration.h"

static MapGenerationProgress* genProgress = nullptr;
static Map* m = nullptr;

// The thread writes to the genProgress variable,
// the polling method reads from it, copies the current value over, 
// so we do not have any overlap in the rendering. 
static int MapGenThread(void* ptr)
{
	// First pass to generate land and water tiles
	for (int y = 0; y < m->height ; y++) {
		for (int x = 0; x < m->width; x++) {
			if (y % 3 == 0 && x % 6 == 0) {
				m->terrainTypeLayer[x + m->width * y] = TerrainType::Water;
			}
			else {
				m->terrainTypeLayer[x + m->width * y] = TerrainType::Land;
			}
		}
	}

	genProgress->currentStep = genProgress->maxSteps;


	// todo(gru) probably copy all data over
	genProgress->finishedMap = m;

	// todo(gru): all kinds of passes for terrain type refinement, resource node generation
	// etc.
	
	return 0;
}


MapGenerationProgress* pollMapGenerationProgress() {
	return genProgress;
}

MapGenerationProgress* generateMap(int w, int h) {
	if (genProgress) {
		delete(genProgress);
	}
	genProgress = new MapGenerationProgress();
	genProgress->maxSteps = 10;
	genProgress->currentStep = 0;
	genProgress->stepName = "Initializing";
	m = new Map();
	m->width = w;
	m->height = h;
	m->terrainTypeLayer = (TerrainType*)malloc(sizeof(TerrainType) * w * h);

	SDL_Thread* thread;
	int         threadReturnValue;

	thread = SDL_CreateThread(MapGenThread, "MapGenThread", (void*)m);

	return genProgress;

}
