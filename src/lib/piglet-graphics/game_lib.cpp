#include "game_lib.h"
#include <cmath>

Vec3 translate(const Vec3& inVector, const Vec3& translationVector)
{
	return inVector + translationVector;
}

Vec3 rotateAroundZ(const Vec3& inVec, float angleRad)
{
	return{ (float)(inVec.x * cos(angleRad) - inVec.y * sin(angleRad)), (float)(inVec.x * sin(angleRad) + inVec.y * cos(angleRad)), inVec.z };
}

Vec3 rotateAroundY(const Vec3& inVec, float angleRad)
{
	return{ (float)(inVec.x * cos(angleRad) + inVec.z * sin(angleRad)), inVec.y, (float)(inVec.x * -sin(angleRad) + inVec.z * cos(angleRad)) };
}



Vec3 perspectiveProject(const Vec3& input, float hFov, float w, float h) {
	float d = w * 0.5 * tan(hFov / 2);
	float ar = w / h;
	return { input.x * d / input.z, input.y * d * ar / input.z, input.z };
}

Vec3 toScreenCoordinates(const Vec3& input, int screenW, int screenH) {
	return { (input.x + 1) * (screenW / 2), screenH - (input.y + 1) * (screenH / 2), 0 };
}
