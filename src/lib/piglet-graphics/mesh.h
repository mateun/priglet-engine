#pragma once
#include <string>
#include <map>
#include <GL/glew.h>
#include <pmath.h>
#include "pgraph_api.h"


	struct PIGLETGRAPHICS_API Mesh {

		GLuint vao;
		int numberOfVertices;
		AABBNoHeight aabb;


	};

PIGLETGRAPHICS_API	std::map<std::string, Mesh*>* getMeshCache();

