//
// Created by martin on 20.09.20.
//

#include "image.h"
#include "texture.h"

Image::Image(GLuint textureId, int w, int h) : _widthInPixels(w), _heightInPixels(h) {
    _texture = textureId;
}

Image::Image(const std::string &fileName, int width, int height)
    : _widthInPixels(width),
      _heightInPixels(height){
    this->_texture = createTexture(fileName);
}

