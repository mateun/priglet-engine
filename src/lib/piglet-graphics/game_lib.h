#pragma once
#include <Windows.h>

struct Vec3 {
	float x;
	float y;
	float z;

	Vec3 operator+(const Vec3& other) const {
		return { other.x + this->x, other.y + this->y, other.z + this->z };
	}

	Vec3 operator-(const Vec3& other) const {
		return { this->x - other.x, this->y - other.y, this->z - other.z };
	}

	Vec3 operator*(const int& factor) {
		return { this->x * factor, this->y * factor, this->z * factor };
	}
};

Vec3 translate(const Vec3& inVector, const Vec3& translationVector);

Vec3 rotateAroundZ(const Vec3& inVec, float angleRad);

Vec3 rotateAroundY(const Vec3& inVec, float angleRad);

Vec3 perspectiveProject(const Vec3& input, float hFov, float w, float h);

Vec3 toScreenCoordinates(const Vec3& input, int screenW, int screenH);

enum TimingGranularity {
	milliseconds,
	microseconds
};


struct Timer {
	LARGE_INTEGER s;
	LARGE_INTEGER e;
	float time = 0;
	LARGE_INTEGER freq;

	Timer() {
		QueryPerformanceFrequency(&freq);
	}

	void start() {
		QueryPerformanceCounter(&s);
	}

	LONGLONG stop() {
		QueryPerformanceCounter(&e);
		time = (float)((e.QuadPart - s.QuadPart) / (float)freq.QuadPart);
		return time * 1000 * 1000;
	}

	float get_last_measure(TimingGranularity tg) {
		if (tg == milliseconds) {
			return time * 1000;
		}

		if (tg == microseconds) {
			return time * 1000 * 1000;
		}
	}
};